#!/usr/bin/env python3

import os
import sys
import time
import logging
import json
import random
import threading
import platform
import urllib.request
import re
from math import cos, sin, fabs, pi
from ev3messages import EV3Messages

from enum import Enum
from agt import AlexaGadget

from ev3dev2.led import Leds
from ev3dev2.sound import Sound

# Set the logging level to INFO to see messages from AlexaGadget
logging.basicConfig(level=logging.INFO, stream=sys.stdout, format='%(message)s')
logging.getLogger().addHandler(logging.StreamHandler(sys.stderr))
logger = logging.getLogger(__name__)

# The BT MAC address of the EV3g device
ev3g_mac = '00:16:53:4F:AF:E7'

class EventName(Enum):
    """
    The list of custom event name sent from this gadget
    """

    Speak = "Speak"

class Plott3rStatus(Enum):
    """
    Class to encapsulate the possible Plott3r statuses
    """
    UNKNOWN = 0
    IDLE    = 1
    BUSY    = 2

class MindstormsGadget(AlexaGadget):
    """
    A Mindstorms gadget that performs movement based on voice commands.
    """

    def __init__(self):
        """
        Performs Alexa Gadget initialization routines and ev3dev resource allocation.
        """
        super().__init__()

        # Ev3dev initialization
        self.leds        = Leds()
        self.sound       = Sound()
        self.msg_handler = EV3Messages(ev3g_mac)

    def __del__(self):
        """
        Anything needing doing on shutdown
        """
        self.msg_handler.stop()

    def on_connected(self, device_addr):
        """
        Gadget connected to the paired Echo device.
        :param device_addr: the address of the device we connected to
        """
        self.leds.set_color("LEFT", "GREEN")
        self.leds.set_color("RIGHT", "GREEN")
        logger.info("{} connected to Echo device".format(self.friendly_name))

    def on_disconnected(self, device_addr):
        """
        Gadget disconnected from the paired Echo device.
        :param device_addr: the address of the device we disconnected from
        """
        self.leds.set_color("LEFT", "BLACK")
        self.leds.set_color("RIGHT", "BLACK")
        logger.info("{} disconnected from Echo device".format(self.friendly_name))

    def on_custom_mindstorms_gadget_control(self, directive):
        """
        Handles the Custom.Mindstorms.Gadget control directive.
        :param directive: the custom directive with the matching namespace and name
        """
        try:
            payload = json.loads(directive.payload.decode("utf-8"))
            print("Control payload: {}".format(payload), file=sys.stderr)
            action = payload["action"]

            if action == "status":
                self.status()
            elif action == "picture":
                pictureID = payload["pictureID"]
                self.picture(pictureID)
            elif action == "text":
                text = payload["payload"]
                self.text(text)
            elif action == "sudoku":
                level = payload["level"] if 'level' in payload else 1
                pid   = payload["pid"]   if 'pid'   in payload else None
                self.sudoku(level=level, pid=pid)
            elif action == "map":
                country  = payload["name"]
                filename = payload["file"]
                self.map(country, filename)
            else:
                self._send_event(EventName.Speak, {
                    'speech': "EV3 Received unknown {} event".format(action)
                })

        except KeyError:
            print("Missing expected parameters: {}".format(directive), file=sys.stderr)

    def picture(self,pictureID):
        """
        Draw out the picture requested
        """
        busy = self._status()

        if busy == Plott3rStatus.IDLE:
            try:
                self._send_mailbox("Command", pictureID)
                mailbox = self._recv_mailbox("ACK")
                if mailbox == None:
                    self._send_event(EventName.Speak,
                        {'speech': "Unexpected empty message received from plotter"})
            except Exception as e:
                print(e, file=sys.stderr)

    def _send_text(self, text):
        """
        Thread method for sending the text to the Plott3r
        """
        print("Starting to send text: {}".format(text), file=sys.stderr)

        try:
            self._send_mailbox("Start", platform.node())
            self._send_mailbox("TextSize", 2)
            self._send_mailbox("Length", len(text))
            for c in text:
                #print(ord(c), file=sys.stderr)
                self._send_mailbox("Data", ord(c))
                mailbox = self._recv_mailbox("ACK")
                if mailbox == None:
                    self._send_event(EventName.Speak,
                        {'speech': "Unexpected empty message received from plotter"})
                    raise OSError("Unexpected message from EV3g") from None
            self._send_mailbox("Print", platform.node())
        except Exception as e:
            print(e, file=sys.stderr)

        print("Finished sending text: {}".format(text), file=sys.stderr)

    def text(self,text):
        """
        Write out the given text
        """
        busy = self._status()

        if busy == Plott3rStatus.IDLE:
            try:
                self._send_mailbox("Command", "TEXT")
                mailbox = self._recv_mailbox("ACK")
                if mailbox != None:
                    # Do this in a thread so that we can handle
                    # other requests from Alexa asap.
                    thread = threading.Thread(target=self._send_text, args=(text,))
                    thread.start()
                else:
                    self._send_event(EventName.Speak,
                        {'speech': "Unexpected empty message received from plotter"})
            except Exception as e:
                print(e, file=sys.stderr)

    def _send_map(self,country,filename):
        """
        Get the map data for the given file and scale it to something that can be plotted
        Do this in a thread for timing reasons
        """

        def _LongLatRanges(polygon):
            """
            Get the range of longitude values
            """
            for inner in polygon:
                for coord in inner:
                    longitudes.append(coord[0])
                    latitudes.append(coord[1])

        def _Polygon(polygon):
            """
            Restructure the polygon data
            
            Change to a non-equatorial orthographic projection, centred on the map region
            """
            for inner in polygon:
                new_poly = []
                for coord in inner:
                    coord[0] = coord[0] * to_rad
                    coord[1] = coord[1] * to_rad
                    x = 6000 * (cos(coord[1]) * sin(coord[0] - midLong))
                    y = 6000 * (cos(midLat) * sin(coord[1]) - sin(midLat) * cos(coord[1]) * cos(coord[0] - midLong))
                    xCoords.append(x)
                    yCoords.append(y)
                    new_poly.append([x, y])

                polygons.append(new_poly)

        URL        = 'https://raw.githubusercontent.com/AshKyd/geojson-regions/master/countries/50m/'
        geodata    = None
        to_rad     = pi/180

        print("Starting to process map: {}".format(country), file=sys.stderr)

        try:
            httpobj = urllib.request.urlopen("{}{}".format(URL,filename))
            geojson = httpobj.read().decode('utf-8')
            geodata = json.loads(geojson)
            geotype = geodata['geometry']['type']
            coords  = geodata['geometry']['coordinates']

        except Exception as e:
            # We have a problem - let the user know
            self._send_event(EventName.Speak, {'speech': "Failed to get map data."})
            print("Failed to access map data file: {}".format(filename), file=sys.stderr)
            exit()

        # Get the range of longitudes
        longitudes = []
        latitudes  = []
        if geotype == 'Polygon':
            _LongLatRanges(coords)
        else:
            for subpoly in coords:
                _LongLatRanges(subpoly)

        minLong = min(longitudes)
        maxLong = max(longitudes)
        minLat  = min(latitudes)
        maxLat  = max(latitudes)
        midLong = (maxLong + minLong)/2 * to_rad
        midLat  = (maxLat  + minLat )/2 * to_rad

        # Remap to the centre of the map region
        xCoords  = []
        yCoords  = []
        polygons = []
        if geotype == 'Polygon':
            _Polygon(coords)
        else:
            for subpoly in coords:
                _Polygon(subpoly)

        # Get the new longitude, and latitude, range
        minX = min(xCoords)
        maxX = max(xCoords)
        minY = min(yCoords)
        maxY = max(yCoords)

        xScale = 170 / (maxX - minX)
        yScale = 240 / (maxY - minY)
        scale  = min(xScale, yScale)
        offset = int((170 - scale*(maxX - minX))/2)

        # This can take ages, so make a progress bar
        total_points = len(xCoords)
        step = int(total_points / 78)

        try:
            start_time = time.time()
            self._send_mailbox("Start", platform.node())
            tally = 0
            for polygon in polygons:
                prev_point = None
                for p in polygon:
                    new_point = [
                        int((maxX-p[0])*scale) + offset,
                        int((maxY-p[1])*scale)
                    ]

                    # Encode the pen & X+Y in a single number
                    if new_point != prev_point:
                        tally += 1
                        if tally % step == 0:
                            print('[', '#' * int(tally/step), '.' * int(78 - tally/step), ']',
                                sep='', end="\r", file=sys.stderr, flush=True)
                        value = new_point[0] + 256*new_point[1] + (0 if prev_point else 65536)
                        self._send_mailbox("PXY", value)

                        mailbox = self._recv_mailbox("ACK")
                        if mailbox == None:
                            self._send_event(EventName.Speak,
                                {'speech': "Unexpected empty message received from plotter"})
                            raise OSError("Unexpected message from EV3g") from None

                        prev_point = new_point
                    else:
                        # Ignore this point and adjust for the progress bar
                        total_points -= 1
                        step = int(total_points / 78)


            # Send the terminator as -1
            self._send_mailbox("PXY", -1)
            print("\nTook {} second(s) to send map data".format(time.time() - start_time), file=sys.stderr)

            # Write the text at the top of the page
            self._send_text(country)

        except Exception as e:
            print(e, file=sys.stderr)

    def map(self,country,filename):
        """
        Draw the requested map
        """
        busy = self._status()

        if busy == Plott3rStatus.IDLE:
            try:
                self._send_mailbox("Command", "MAP")
                mailbox = self._recv_mailbox("ACK")
                if mailbox != None:
                    # Do this in a thread so that we can handle
                    # other requests from Alexa asap.
                    thread = threading.Thread(target=self._send_map, args=(country,filename,))
                    thread.start()
                else:
                    self._send_event(EventName.Speak,
                        {'speech': "Unexpected empty message received from plotter"})
            except Exception as e:
                print(e, file=sys.stderr)

    def _send_sudoku(self, level=1, sid=None):
        try:
            url = "https://nine.websudoku.com/?level={}{}".format(
                level, ('&set_id='+sid if sid != None else ''))

            print(url,file=sys.stderr)

            httpobj = urllib.request.urlopen(url)
            html    = httpobj.read().decode('utf-8')
            pid     = re.search(r'ID="pid"[^>]*VALUE="(\d+)"', html)
            cheat   = re.search(r'ID="cheat"[^>]*VALUE="(\d+)"', html)

            pid     = "ID: {}".format(pid.group(1))
            cheat   = cheat.group(1)

            boxes = []
            if sid != None:
                for i,v in enumerate(cheat):
                    boxes.append(int(v) * 100 + 10*int(i/9) + i%9)
            else:
                tags  = re.split(r'<', html)
                clues = list(filter(lambda x:
                    re.search(r'VALUE="\d"[^>]*ID=f\d\d', x), tags))
                for clue in clues:
                    info = re.search(r'VALUE="(\d)".*ID=f(\d\d)', clue)
                    v,rc = info.group(1,2)
                    boxes.append(int(v+rc))

            print("Len {}".format(len(boxes)),file=sys.stderr)
            print("VRC {}".format(boxes),file=sys.stderr)
            print("PID {}".format(pid),file=sys.stderr)
            print("Cheat {}".format(cheat),file=sys.stderr)

            # Send the sudoku as "text"
            self._send_mailbox("Start", platform.node())
            self._send_mailbox("TextSize", 2)
            self._send_mailbox("Length", len(boxes))
            for b in boxes:
                self._send_mailbox("Data", b)
                mailbox = self._recv_mailbox("ACK")
                if mailbox == None:
                    self._send_event(EventName.Speak,
                        {'speech': "Unexpected empty message received from plotter"})
                    raise OSError("Unexpected message from EV3g") from None
            self._send_mailbox("Print", platform.node())

            # Send the Puzzle ID
            self._send_mailbox("Start", platform.node())
            self._send_mailbox("TextSize", 2)
            self._send_mailbox("Length", len(pid))
            for c in pid:
                self._send_mailbox("Data", ord(c))
                mailbox = self._recv_mailbox("ACK")
                if mailbox == None:
                    self._send_event(EventName.Speak,
                        {'speech': "Unexpected empty message received from plotter"})
                    raise OSError("Unexpected message from EV3g") from None
            self._send_mailbox("Print", platform.node())

        except Exception as e:
            print(e, file=sys.stderr)

    def sudoku(self,level=1,pid=None):
        """
        Write out the given sudoku
        """
        busy = self._status()

        if busy == Plott3rStatus.IDLE:
            try:
                self._send_mailbox("Command", "SUDOKU")
                mailbox = self._recv_mailbox("ACK")
                if mailbox != None:
                    # Do this in a thread so that we can handle
                    # other requests from Alexa asap.
                    thread = threading.Thread(target=self._send_sudoku,
                        kwargs={'level': level, 'sid': pid})
                    thread.start()
                else:
                    self._send_event(EventName.Speak,
                        {'speech': "Unexpected empty message received from plotter"})
            except Exception as e:
                print(e, file=sys.stderr)

    def status(self):
        """
        React to the status voice request
        """
        busy = self._status()

        if busy == Plott3rStatus.IDLE:
            self._send_event(EventName.Speak, {'speech': "Plotter is idle"})
            
    def _status(self):
        """
        Get the status of the EV3g brick
        """
        status = Plott3rStatus.UNKNOWN

        try:
            print("{}: Sending my name '{}' to plotter".format(time.asctime(), platform.node()), file=sys.stderr)

            self._send_mailbox("Status", platform.node())
            mailbox = self._recv_mailbox("Busy", 5)

            if mailbox != None:
                status = Plott3rStatus.BUSY if mailbox.value else Plott3rStatus.IDLE
        except Exception as e:
            print(e, file=sys.stderr)

        if status == Plott3rStatus.UNKNOWN:
            self._send_event(EventName.Speak, {'speech': "Plotter state is unknown"})

        if status == Plott3rStatus.BUSY:
                self._send_event(EventName.Speak, {'speech': "Plotter is busy"})

        return status
        
    def _send_event(self, name: EventName, payload):
        """
        Sends a custom event to back to Alexa
        :param name: the name of the custom event
        :param payload: the JSON payload 
        """
        self.send_custom_event('Custom.Mindstorms.Gadget', name.value, payload)

    def _bt_connect(self):
        """
        Ensure that the EV3g brick is connected.
        """
        if self.bt_socket == None:
            bt_socket = bluetooth.BluetoothSocket(bluetooth.RFCOMM)

            print("{}: Connection attempt to {}".format(time.asctime(), ev3g_mac), file=sys.stderr)
            try:
                bt_socket.connect((ev3g_mac, 1))
                bt_socket.settimeout(5)
                self.bt_socket = bt_socket
                print("{}: BT Connected".format(time.asctime()), file=sys.stderr)
            except:
                print("{}: BT failed to connect".format(time.asctime()), file=sys.stderr)
                self._send_event(EventName.Speak, {'speech': "Failed to connect to EV3 g"})
                raise OSError("Failed to connect to EV3g") from None

    def _send_mailbox(self, name, value):
        """
        Send a mailbox message to the EV3g brick
        """
        try:
            self.msg_handler.send(name, value)
        except:
            self._send_event(EventName.Speak, {'speech': "Failed to send to EV3 g"})
            raise

    def _recv_mailbox(self, name, timeout=None):
        """
        Receive a mailbox message from the EV3g brick
        """

        return self.msg_handler.get(name, timeout)
        try:
            self._bt_connect()
        except:
            raise
        
if __name__ == '__main__':

    gadget = MindstormsGadget()

    # Set LCD font and turn off blinking LEDs
    os.system('setfont Lat7-Terminus12x6')
    gadget.leds.set_color("LEFT", "BLACK")
    gadget.leds.set_color("RIGHT", "BLACK")

    # Startup sequence
    gadget.sound.play_song((('C4', 'e'), ('D4', 'e'), ('E5', 'q')))
    gadget.leds.set_color("LEFT", "GREEN")
    gadget.leds.set_color("RIGHT", "GREEN")

    # Gadget main entry point
    gadget.main()

    # Shutdown sequence
    gadget.sound.play_song((('E5', 'e'), ('C4', 'e')))
    gadget.leds.set_color("LEFT", "BLACK")
    gadget.leds.set_color("RIGHT", "BLACK")
