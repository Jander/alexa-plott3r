/*
 * Copyright 2019 Jerry Nicholls
 *
 * Based upon mission 03 from the LEGO/Alexa example code
*/

const Alexa = require('ask-sdk-core');
const Util = require('./util');
const Common = require('./common');
const Https = require('https'); 

// The namespace of the custom directive to be sent by this skill
const NAMESPACE = 'Custom.Mindstorms.Gadget';

// The name of the custom directive to be sent this skill
const NAME_CONTROL = 'control';

const LaunchRequestHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'LaunchRequest';
    },
    handle: async function(handlerInput) {

        let request = handlerInput.requestEnvelope;
        let { apiEndpoint, apiAccessToken } = request.context.System;
        let apiResponse = await Util.getConnectedEndpoints(apiEndpoint, apiAccessToken);
        if ((apiResponse.endpoints || []).length === 0) {
            return handlerInput.responseBuilder
            .speak(`I could not find a connected EV3 brick. Please check that your EV3 brick is connected, and try again.`)
            .getResponse();
        }

        // Store the gadget endpointId to be used in this skill session
        let endpointId = apiResponse.endpoints[0].endpointId || [];
        Util.putSessionAttribute(handlerInput, 'endpointId', endpointId);
        
        // Explicitly set duration to zero, so that it doesn't get extended
        Util.putSessionAttribute(handlerInput, 'duration', 0);

        // Set the token to track the event handler
        const token = handlerInput.requestEnvelope.request.requestId;
        Util.putSessionAttribute(handlerInput, 'token', token);

        return handlerInput.responseBuilder
            .speak("Hello. Please tell me what you want the plotter to do.")
            .reprompt("Please tell me what to draw or write")
            .getResponse();
    }
};

// Make the Plott3r draw the requested picture
const PictureIntentHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'PictureIntent';
    },
    handle: function (handlerInput) {
        const request     = handlerInput.requestEnvelope;
        
        // Get the picture type request, and its ID
        const picture     = Alexa.getSlotValue(request, 'PictureType');
        const pictureSlot = Alexa.getSlot(request, 'PictureType');
        const pictureID   = pictureSlot.resolutions.resolutionsPerAuthority[0].values[0].value.id;

        const attributesManager = handlerInput.attributesManager;
        const endpointId = attributesManager.getSessionAttributes().endpointId || [];

        // Construct the directive with the payload containing the parameters
        const directive = Util.build(endpointId, NAMESPACE, NAME_CONTROL,
            {
                action: 'picture',
                picture: picture,
                pictureID: pictureID,
            });

        const speechOutput = `Requesting the plotter to draw a ${picture}`;
        
        let sessionAttributes = attributesManager.getSessionAttributes();

        return handlerInput.responseBuilder
            .speak(speechOutput)
            .reprompt("Waiting for a plotter command.")
            .addDirective(directive)
            .addDirective(Util.buildStartEventHandler(sessionAttributes.token, 30000, {})) // Restart the event handler
            .getResponse();
    }
};

// Make the Plott3r write out the requested list
const ListIntentHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'ListIntent';
    },
    async handle(handlerInput) {
        const request  = handlerInput.requestEnvelope;
        let speechOutput;
        
        // Get the list type request, and its ID
        const list     = Alexa.getSlotValue(request, 'ListType');
        const listSlot = Alexa.getSlot(request, 'ListType');
        const listID   = listSlot.resolutions.resolutionsPerAuthority[0].values[0].value.id;

        const attributesManager = handlerInput.attributesManager;
        const endpointId = attributesManager.getSessionAttributes().endpointId || [];
            
        const amazonList = await GetListItems(handlerInput, listID);
        
        if (amazonList === null) {
            speechOutput = 'List read permission has not been enabled for this skill. You should grant permissions within the Alexa app.';
            return handlerInput.responseBuilder
                .speak(speechOutput)
                .reprompt("Waiting for a plotter command.")
                .getResponse();
        }
        
        if (amazonList === '') {
            speechOutput = `No items to print from your ${list} list`;
            return handlerInput.responseBuilder
                .speak(speechOutput)
                .reprompt("Waiting for a plotter command.")
                .getResponse();
        }
        
        // Construct the directive with the payload containing the parameters
        const directive = Util.build(endpointId, NAMESPACE, NAME_CONTROL,
            {
                action:  'text',
                list:    list,      // The list and ID aren't used, but send them anyway.
                listID:  listID,
                payload: amazonList
            });

        speechOutput = `Requesting the plotter to write out your ${list} list, which is ${amazonList}`;

        let sessionAttributes = attributesManager.getSessionAttributes();

        return handlerInput.responseBuilder
            .speak(speechOutput)
            .reprompt("Waiting for a plotter command.")
            .addDirective(directive)
            .addDirective(Util.buildStartEventHandler(sessionAttributes.token, 30000, {})) // Restart the event handler
            .getResponse();
    }
};

// Make the Plott3r write out the requested memo
const MemoIntentHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'MemoIntent';
    },
    async handle(handlerInput) {
        const request  = handlerInput.requestEnvelope;
        let speechOutput;
        
        // Get the memo request
        const memo = Alexa.getSlotValue(request, 'Memo');

        const attributesManager = handlerInput.attributesManager;
        const endpointId = attributesManager.getSessionAttributes().endpointId || [];

        let sessionAttributes = attributesManager.getSessionAttributes();

        if (request.request.intent.confirmationStatus !== "DENIED") {
            speechOutput = `Requesting the plotter to write out your memo`;

            // Construct the directive with the payload containing the parameters
            const directive = Util.build(endpointId, NAMESPACE, NAME_CONTROL,
                {
                    action:  'text',
                    payload: memo
                });

            return handlerInput.responseBuilder
                .speak(speechOutput)
                .reprompt("Waiting for a plotter command.")
                .addDirective(directive)
                .addDirective(Util.buildStartEventHandler(sessionAttributes.token, 30000, {})) // Restart the event handler
                .getResponse();
        } else {
            speechOutput = `Cancelling your request to take a memo`;

            return handlerInput.responseBuilder
                .speak(speechOutput)
                .reprompt("Waiting for a plotter command.")
                .addDirective(Util.buildStartEventHandler(sessionAttributes.token, 30000, {})) // Restart the event handler
                .getResponse();
        }
    }
};

// Make the Plott3r draw out the outline of the requested country
const MapIntentHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'MapIntent';
    },
    async handle(handlerInput) {
        const request  = handlerInput.requestEnvelope;
        let speechOutput;

        // Get the country request
        const country   = Alexa.getSlotValue(request, 'Country');
        const indexData = await GetGeoJSON('index.json');

        // Attempt to find the Country
        if (!indexData) {
            return handlerInput.responseBuilder
                .speak("There was a problem getting map data")
                .reprompt("Waiting for a plotter command.")
                .addDirective(Util.buildStartEventHandler(sessionAttributes.token, 30000, {})) // Restart the event handler
                .getResponse();
        }

        let mapFile;
        let mapName;
        Object.keys(indexData.all).forEach(function(key) {
            if (country.toUpperCase() === indexData.all[key].countryName.toUpperCase()) {
                console.log(`Country KEY = ${key}, CountryName = ${indexData.all[key].countryName}, FileName = ${indexData.all[key].filename}`);
                mapFile = indexData.all[key].filename;
                mapName = indexData.all[key].countryName;
            }
        });

        const attributesManager = handlerInput.attributesManager;
        const endpointId = attributesManager.getSessionAttributes().endpointId || [];

        let sessionAttributes = attributesManager.getSessionAttributes();

        if (!mapFile) {
            return handlerInput.responseBuilder
                .speak(`I could not find a map file for ${country}, sorry.`)
                .reprompt("Waiting for a plotter command.")
                .addDirective(Util.buildStartEventHandler(sessionAttributes.token, 30000, {})) // Restart the event handler
                .getResponse();
        }


        // Construct the directive with the payload containing the parameters
        const directive = Util.build(endpointId, NAMESPACE, NAME_CONTROL,
            {
                action: 'map',
                name: mapName,
                file: mapFile,
            });

        console.log(JSON.stringify(directive));

        speechOutput = `Asking the plotter to draw the outline of ${country}`;

        return handlerInput.responseBuilder
            .speak(speechOutput)
            .reprompt("Waiting for a plotter command.")
            .addDirective(directive)
            .addDirective(Util.buildStartEventHandler(sessionAttributes.token, 30000, {})) // Restart the event handler
            .getResponse();
    }
};

// Make the Plott3r draw the requested sudoku
const SudokuIntentHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && (Alexa.getIntentName(handlerInput.requestEnvelope) === 'NewSudokuIntent' ||
                Alexa.getIntentName(handlerInput.requestEnvelope) === 'OldSudokuIntent');

    },
    handle: function (handlerInput) {
        const request     = handlerInput.requestEnvelope;

        // Get the picture type request, and its ID
        const pid   = Alexa.getSlotValue(request, 'PID');
        const level = Alexa.getSlotValue(request, 'Difficulty');

        // Construct the directive with the payload containing the parameters
        let directive;
        let levelID = 1;

        console.log(`Base Sudoku level ${level} ID ${levelID}`);

        if (level) {
            const levelSlot = Alexa.getSlot(request, 'Difficulty');
            levelID = levelSlot.resolutions.resolutionsPerAuthority[0].values[0].value.id || 1;
            console.log(`Sudoku level ${level} ID ${levelID}`);
        }

        const attributesManager = handlerInput.attributesManager;
        const endpointId = attributesManager.getSessionAttributes().endpointId || [];

        directive = Util.build(endpointId, NAMESPACE, NAME_CONTROL,
            {
                action: 'sudoku',
                pid: pid,
                level: levelID
            });

        const speechOutput = `Requesting the plotter to draw your Sudoku`;

        let sessionAttributes = attributesManager.getSessionAttributes();

        return handlerInput.responseBuilder
            .speak(speechOutput)
            .reprompt("Waiting for a plotter command.")
            .addDirective(directive)
            .addDirective(Util.buildStartEventHandler(sessionAttributes.token, 30000, {})) // Restart the event handler
            .getResponse();
    }
};

// Ask the Plott3r for its status
const StatusIntentHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
            && Alexa.getIntentName(handlerInput.requestEnvelope) === 'StatusIntent';
    },
    handle: function (handlerInput) {
        const request     = handlerInput.requestEnvelope;
        
        const attributesManager = handlerInput.attributesManager;
        const endpointId = attributesManager.getSessionAttributes().endpointId || [];

        // Construct the directive with the payload containing the parameters
        const directive = Util.build(endpointId, NAMESPACE, NAME_CONTROL,
            {
                action: 'status',
            });

        const speechOutput = `Requesting the plotter status`;
        
        let sessionAttributes = attributesManager.getSessionAttributes();

        return handlerInput.responseBuilder
            .speak(speechOutput)
            .reprompt("Waiting for a plotter command.")
            .addDirective(directive)
            .addDirective(Util.buildStartEventHandler(sessionAttributes.token, 30000, {})) // Restart the event handler
            .getResponse();
    }
};

// Receive text back from ev3dev and just say it
const EventsReceivedRequestHandler = {
    // Checks for a valid token and endpoint.
    canHandle(handlerInput) {
        let { request } = handlerInput.requestEnvelope;
        console.log('Request type: ' + Alexa.getRequestType(handlerInput.requestEnvelope));
        if (request.type !== 'CustomInterfaceController.EventsReceived') return false;

        const attributesManager = handlerInput.attributesManager;
        let sessionAttributes = attributesManager.getSessionAttributes();
        let customEvent = request.events[0];

        // Validate event token
        if (sessionAttributes.token !== request.token) {
            console.log("Event token doesn't match. Ignoring this event");
            return false;
        }

        // Validate endpoint
        let requestEndpoint = customEvent.endpoint.endpointId;
        if (requestEndpoint !== sessionAttributes.endpointId) {
            console.log("Event endpoint id doesn't match. Ignoring this event");
            return false;
        }
        return true;
    },
    handle(handlerInput) {

        console.log("== Received Custom Event ==");
        let customEvent = handlerInput.requestEnvelope.request.events[0];
        let payload     = customEvent.payload;
        let name        = customEvent.header.name;

        let speechOutput;
        if (name === 'Speak') {
            speechOutput = payload.speech;

        } else {
            speechOutput = "Unknown response from the plotter.";
        }
        return handlerInput.responseBuilder
            .speak(speechOutput)
            .getResponse();
    }
};

// When the event handler expires, consider extending it in blocks of 30s.
const ExpiredRequestHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'CustomInterfaceController.Expired'
    },
    handle(handlerInput) {
        console.log("== Custom Event Expiration Input ==");

        // Set the token to track the event handler
        const token = handlerInput.requestEnvelope.request.requestId;
        Util.putSessionAttribute(handlerInput, 'token', token);

        const attributesManager = handlerInput.attributesManager;
        let duration = attributesManager.getSessionAttributes().duration || 0;
        if (duration > 0) {
            Util.putSessionAttribute(handlerInput, 'duration', --duration);

            // Extends skill session
            return handlerInput.responseBuilder
                //.speak(`Extending event handler. Duration value is ${duration}`)
                .addDirective(Util.buildStartEventHandler(token, 30000, {}))
                .getResponse();
        }
        else {
            // End skill session
            return handlerInput.responseBuilder
                .speak("Closing LEGO plotter skill. Goodbye.")
                .withShouldEndSession(true)
                .getResponse();
        }
    }
};

// Method to get the GeoJSON data from https://github.com/AshKyd/geojson-regions/tree/master/countries/110m
function GetGeoJSON(query) {
  return new Promise(((resolve, reject) => {
    var options = {
        host: 'raw.githubusercontent.com',
        path: '/AshKyd/geojson-regions/master/countries/110m/' + encodeURIComponent(query),
        method: 'GET',
    };

    const request = Https.request(options, (response) => {
      response.setEncoding('utf8');
      let returnData = '';

      response.on('data', (chunk) => {
        returnData += chunk;
      });

      response.on('end', () => {
        resolve(JSON.parse(returnData));
      });

      response.on('error', (error) => {
        reject(error);
      });
    });
    request.end();
  }));
}

/*
* Get the list ID for the list matching the suffix
*/
async function _getListId(handlerInput, suffix) {
  // check session attributes to see if it has already been fetched
  const attributesManager = handlerInput.attributesManager;
  let   sessionAttributes = attributesManager.getSessionAttributes();
  let   listId = null;
  
  console.log(`Looking for a list ending in ${suffix}`)
  
  if (!sessionAttributes.suffix) {
    // lookup the id for the list matching the suffix
    const listClient = handlerInput.serviceClientFactory.getListManagementServiceClient();
    const listOfLists = await listClient.getListsMetadata();

    for (let i = 0; i < listOfLists.lists.length; i += 1) {
      console.log(`found ${listOfLists.lists[i].name} with id ${listOfLists.lists[i].listId}`);
      const decodedListId = Buffer.from(listOfLists.lists[i].listId, 'base64').toString('utf8');
      console.log(`decoded listId: ${decodedListId}`);
      // The default lists (To-Do and Shopping List) list_id values are base-64 encoded strings with these formats:
      //  <Internal_identifier>-TASK for the to-do list
      //  <Internal_identifier>-SHOPPING_LIST for the shopping list
      // Developers can base64 decode the list_id value and look for the specified string at the end. This string is constant and agnostic to localization.
      if (decodedListId.endsWith(`-${suffix}`)) {
        console.log(`MATCHED listId: ${decodedListId}`);
        // since we're looking for the default to do list, it's always present and always active
        listId = listOfLists.lists[i].listId;
        break;
      }
    }
  }
  
  sessionAttributes[suffix] = listId;

  attributesManager.setSessionAttributes(sessionAttributes);
  console.log(JSON.stringify(handlerInput));
  return sessionAttributes[suffix];
}

// Get the items on the requested list.
async function GetListItems(handlerInput, suffix) {
  let listClient;
  let listId = null;
  
  try {
    listClient = handlerInput.serviceClientFactory.getListManagementServiceClient();
    listId     = await _getListId(handlerInput, suffix);
  }
  catch(error) {
    console.log('permissions are not defined');
    return null;
  }
  
  console.log(`listid: ${listId}`);
  if (!listId) {
      console.log(`No list found ending in -${suffix}`);
      return '';
  }
  
  const list = await listClient.getList(listId, 'active');
  if (!list) {
    console.log('null list');
    return '';
  }
  else if (!list.items || list.items.length === 0) {
    console.log('empty list');
    return '';
  }
  
  const _valueReducer = (acc, cur) => acc + cur.value + "\n";
  const listContents  = list.items.reduce(_valueReducer, '');
  return listContents;
}

// Skill routing
exports.handler = Alexa.SkillBuilders.custom()
    .addRequestHandlers(
        LaunchRequestHandler,
        PictureIntentHandler,
        ListIntentHandler,
        MemoIntentHandler,
        MapIntentHandler,
        SudokuIntentHandler,
        StatusIntentHandler,
        EventsReceivedRequestHandler,
        ExpiredRequestHandler,
        Common.HelpIntentHandler,
        Common.CancelAndStopIntentHandler,
        Common.SessionEndedRequestHandler,
        Common.IntentReflectorHandler, // Catchall for any missing handlers
    )
    .addRequestInterceptors(Common.RequestInterceptor)
    .withApiClient(new Alexa.DefaultApiClient())
    .withCustomUserAgent('LEGOPlott3r/list-access/v1')
    .addErrorHandlers(
        Common.ErrorHandler,
    )
    .lambda();
